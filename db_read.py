#!/usr/bin/env python3

from sqlalchemy import create_engine
import pymysql
import pandas as pd
import os
import numpy as np

# import datetime
from datetime import timezone
from datetime import datetime 
from datetime import timedelta


DB_USER = os.environ.get('DB_USER')
DB_DATABASE = os.environ.get('DB_DATABASE')
DB_PASSWORD = os.environ.get('DB_PASSWORD')
DB_HOST = os.environ.get('DB_HOST')
DB_PORT = os.environ.get('DB_PORT')

connectString = "mysql://"
connectString += DB_USER + ":"
connectString += DB_PASSWORD + "@"
connectString += DB_HOST + ":"
connectString += DB_PORT + "/"
connectString += DB_DATABASE

print(connectString)
sqlEngine = create_engine(connectString)
dbConnection    = sqlEngine.connect()
air_condition   = pd.read_sql("select * from airlog.comet", dbConnection);
dbConnection.close()

print(air_condition)

