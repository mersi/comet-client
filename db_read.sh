#!/bin/bash

export DB_USER=comet_reader

if [ ! -f "$DB_USER" ] ; then
  echo "Please create a file called $DB_USER containing the password"
  exit -1;
fi

export DB_DATABASE="airlog"
export DB_PASSWORD=`cat $DB_USER`
export DB_HOST=dbod-cms-186-log-8.cern.ch
export DB_PORT=5510

python3 db_read.py

