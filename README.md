
# Comet reader

This program reads the values from one (or more) comet T3611 and archive them to a database
to be later plot via graphana or something similar

- INSTALL should explain how to install all the needed dependencies for this to work

- `db_read.sh` shows reads the database values with python
- `cometRead_dbWrite.sh` reads a measurement from both comet sensors and archives them to the database

### Database table

This is the reference of the database table where we store the results

```
 measTime      | datetime     |
 temperature   | double       |
 dewPoint      | double       |
 rh            | double       |
 devname       | varchar(100) |
 devsn         | int          |
 location      | varchar(100) |
```

## INSTALL and requirements
Installation needs a few python modules
and the creation of the files ```comet_reader``` and ```comet_writer``` with the database password

```bash
pip3 install --user pymysql pandas mysql-connector requests
```

# either this one or the next, depending on the system
# sudo apt install python3-mysqldb
# sudo yum install MySQL-python 


## How to setup a comet
First write down the MAC address, which is written down on the side of the comet, and then register in network.cern.ch
Connect the comet to a LAN. It will get the address ```http://192.168.1.123/```
You should then open the Settings page and change the following settings:
   - Under **General** set ```Device name``` to the room name (ex. 186-1-E11)
   - Under **Network** set ```Obtain an IP address automatically``` to true (from here on you should be able to connect it to the CERN GPN)
   - Under **Time** set
     - ```Time synchronization enabled``` to true
     - ```SNTP server IP address``` to ```137.138.16.69```
     - ```NTP synchronization every hour``` to true
   - Under **WWW and Security** set
     - ```Security enabled``` to true
     - Assign a password for admin 


