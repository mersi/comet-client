#!/bin/bash

myDir=`dirname "$0"`
cd $myDir

export DB_USER=comet_writer

if [ ! -f "$DB_USER" ] ; then
  echo "Please create a file called $DB_USER containing the password"
  exit -1;
fi

export DB_DATABASE="airlog"
export DB_PASSWORD=`cat $DB_USER`
export DB_HOST=dbod-cms-186-log-8.cern.ch
export DB_PORT=5510

# 7 is temporarily disconnected
# it will go in the new dry air cabinet
for i in 1 2 3 4 5 6 7 8 9 10 11 12; do
	export DB_DevName="comet-186-${i}.cern.ch"
	python3 ./cometRead_dbWrite.py
done

for i in 1 2; do
	export DB_DevName="comet-187-${i}.cern.ch"
        python3 ./cometRead_dbWrite.py
done

