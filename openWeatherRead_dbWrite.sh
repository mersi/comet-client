#!/bin/bash

myDir=`dirname -- "$0"`
[ -d "$myDir" ] || myDir=.
cd $myDir

export DB_USER=comet_writer

if [ ! -f "$DB_USER" ] ; then
  echo "Please create a file called $DB_USER containing the password"
  exit -1;
fi

if [ -f ./openWeatherConfig.sh ] ; then
  source ./openWeatherConfig.sh
else
  export OW_API_KEY=""
  echo "Please create a file called openweathermap_api_key containing your openweathermap api key"
fi
 
export DB_DATABASE="airlog"
export DB_PASSWORD=`cat $DB_USER`
export DB_HOST=dbod-cms-186-log-8.cern.ch
export DB_PORT=5510

python3 ./openWeatherRead_dbWrite.py

