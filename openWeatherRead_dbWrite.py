#!/usr/bin/env python3

# from sqlalchemy import create_engine, insert, Table, MetaData
import pymysql
import os
import mysql.connector
import requests
import math

# import datetime
from datetime import timezone
from datetime import datetime 
from datetime import timedelta

import json
import subprocess
from os.path import exists
import sys

DB_USER = os.environ.get('DB_USER')
DB_DATABASE = os.environ.get('DB_DATABASE')
DB_PASSWORD = os.environ.get('DB_PASSWORD')
DB_HOST = os.environ.get('DB_HOST')
DB_PORT = os.environ.get('DB_PORT')
OW_API_KEY = os.environ.get('OW_API_KEY')
OW_TOWN_CODE = os.environ.get('OW_TOWN_CODE')

def get_dew_point(T_K, RH):
    # https://journals.ametsoc.org/view/journals/apme/35/4/1520-0450_1996_035_0601_imfaos_2_0_co_2.xml
    # Improved Magnus Form Approximation of Saturation Vapor Pressure
    # Oleg A. Alduchov and Robert E. Eskridge
    # DOI: https://doi.org/10.1175/1520-0450(1996)035<0601:IMFAOS>2.0.CO;2
    # Using AERK approximation from quoted article
    # partial saturation pressure E_W = C*exp(A*T/(B+T))
    # hence current pressure is RH*C*exp(A*T/(B+T))
    # hence RH*C*exp(A*T/(B+T)) = 100%*exp(A*T_DEW/(B+T_DEW))
    # defining x = log(RH/100%) + A*T/(B+T)
    # T_DEW = x*B/(A-x)
    T = T_K-273.15
    A = 17.625
    B = 243.04
    # C = 6.1094
    x = math.log(RH/100.)+A*T/(B+T)
    dp = x*B/(A-x)
    return dp

connectString = "mysql://"
connectString += DB_USER + ":"
connectString += DB_PASSWORD + "@"
connectString += DB_HOST + ":"
connectString += DB_PORT + "/"
connectString += DB_DATABASE

try:
    # connect to database
    mydb = mysql.connector.connect(
        host = DB_HOST,
        port = DB_PORT,
        user = DB_USER,
        password = DB_PASSWORD,
        database = DB_DATABASE
    )
except:
    print("Could not connect to database")
    exit(-1)

# Create a cursor object
mycursor = mydb.cursor()

# Perform an http request to get the current data point and put the result into the 'dataPoints' variable
apiBaseUrl="https://api.openweathermap.org/data/2.5/"
valuesUrl=apiBaseUrl+"weather?id="+OW_TOWN_CODE+"&appid="+OW_API_KEY
r =requests.get(valuesUrl)
if r.status_code != 200: 
   print("Error: http request to url '%s' failed with status %s" % (valuesUrl, r.status_code) )
   sys.exit(-1)
dataPointString=r.text
dataPoints=json.loads(dataPointString)

# debug json
# prettyDataPointString = json.dumps(dataPoints, indent=4)
# print(prettyDataPointString)

# This time is stored independent of the timezone
# (checked and verified)
epochMeas=int(dataPoints['dt'])
epochCall=datetime.now().timestamp()
myTemp=float(dataPoints['main']['temp'])
myRH=float(dataPoints['main']['humidity'])
myLocation=dataPoints['name']
myDewPoint=get_dew_point(myTemp, myRH)
myTemp-=273.15
# print("dt = %d now = %d  temp = %f  rh=%f dewPoint=%f locationName=%s location=%s"  % (epochMeas, epochCall,  myTemp, myRH, myDewPoint, myLocation, OW_TOWN_CODE))

insertString="INSERT IGNORE into airlog.openWeather"
insertString+=" (measEpoch, measDateTime, callDateTime, temperature_C, rh_percent, dewPoint_C, location, locationName, server)"
insertString+=" values (%s, FROM_UNIXTIME(%s), FROM_UNIXTIME(%s), %s, %s, %s, %s, %s, %s)"
myInsertValues = (epochMeas, epochMeas, epochCall, myTemp, myRH, myDewPoint, OW_TOWN_CODE, myLocation, apiBaseUrl)

# print(insertString)
# print(myInsertValues)

mycursor.execute(insertString, myInsertValues)

mydb.commit()

# nInsertedRecords = mycursor.rowcount
# if nInsertedRecords != 1 :
#     print("ERROR: I found ", mycursor.rowcount, "record inserted instead of 1")

mycursor.close()


