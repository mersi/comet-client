#!/usr/bin/env python3

# This is how to get the datapoints via modbus
# documentation is from this manual:
# https://www.cometsystem.com/userfiles/dokumenty_menu/48/ie-snc-tx6xx.pdf
# this requries installing modbus via:
# pip3 install --user pyModbusTCP

# For the record, we decided to got for a simple json HTTP request, instead, but this might be
# interesting for other applications

from pyModbusTCP.client import ModbusClient 

DB_DevName="comet-186-1.cern.ch"
c = ModbusClient(host=DB_DevName, port=502, unit_id=1, auto_open=True)

def printReg(register) :
    regs = c.read_holding_registers(register, 1)
    if regs:
        print("register %d has value %s" % (register, regs) );
    else:
        print("read error")
def saveReg(register) :
    regs = c.read_holding_registers(register, 1)
    if regs:
        value=regs[0]/10
    else :
        print("read error")
    return value

printReg(48)
printReg(49)
printReg(50)

printReg(0x1035)
printReg(4150)
printReg(4151)
printReg(4152)

