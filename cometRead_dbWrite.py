#!/usr/bin/env python3

# from sqlalchemy import create_engine, insert, Table, MetaData
import pymysql
import os
import mysql.connector
import requests
import socket

# import datetime
from datetime import timezone
from datetime import datetime 
from datetime import timedelta

import json
import subprocess
from os.path import exists
import sys

DB_USER = os.environ.get('DB_USER')
DB_DATABASE = os.environ.get('DB_DATABASE')
DB_PASSWORD = os.environ.get('DB_PASSWORD')
DB_HOST = os.environ.get('DB_HOST')
DB_PORT = os.environ.get('DB_PORT')
DB_DevName = os.environ.get('DB_DevName')

connectString = "mysql://"
connectString += DB_USER + ":"
connectString += DB_PASSWORD + "@"
connectString += DB_HOST + ":"
connectString += DB_PORT + "/"
connectString += DB_DATABASE

try:
    # connect to database
    mydb = mysql.connector.connect(
        host = DB_HOST,
        port = DB_PORT,
        user = DB_USER,
        password = DB_PASSWORD,
        database = DB_DATABASE
    )
except:
    print("Could not connect to database")
    exit(-1)

# Create a cursor object
mycursor = mydb.cursor()

# This time is stored independent of the timezone
# (checked and verified)
myDateTime=datetime.now()
myDevName=DB_DevName

# Define a function to insert invalid data
def insertInvalid(reason):
    insertString="INSERT into airlog.comet"
    insertString+=" (measTime_date, measTime, measTime_ts, temperature, dewPoint, rh, devname, devsn, location, valid)"
    insertString+=" values (%s, FROM_UNIXTIME(%s), %s, %s, %s, %s, %s, %s, %s, %s)"
    myInsertValues = (myDateTime, myDateTime.timestamp(), myDateTime.timestamp(), None, None, None, myDevName, None, None, reason)
    mycursor.execute(insertString, myInsertValues)
    mydb.commit()


# Define a function to detect if the name can be resolved
def can_resolve_hostname(hostname):
    try:
        ip_address = socket.gethostbyname(hostname)
        return True
    except socket.gaierror:
        return False

if not can_resolve_hostname(DB_DevName):
    insertInvalid("CANNOT_RESOLVE")
    # print("Error: cannot resolve host '%s'" % (DB_DevName) )
    sys.exit(-1)

# Perform an http request to get the current data point and put the result into the 'dataPoints' variable
valuesUrl="http://"+DB_DevName+"/values.json"
try:
   r =requests.get(valuesUrl, timeout=3)
   if r.status_code != 200: 
      print("Error: http request to url '%s' failed with status %s" % (valuesUrl, r.status_code) )
      sys.exit(-1)
except requests.exceptions.Timeout:
   insertInvalid('CANNOT_CONNECT')
   # print("Error: timeout in http request to url '%s': uploading a bunch of NONE" % (valuesUrl) )
   sys.exit(-1)

dataPointString=r.text
dataPoints=json.loads(dataPointString)

myDewPoint=float(dataPoints['ch3']['aval'])
myRH=float(dataPoints['ch2']['aval'])
myTemp=float(dataPoints['ch1']['aval'])
myDevSn=float(dataPoints['devsn'])
myLocation=dataPoints['devname']
myUnixTime=dataPoints['timeunix']


myDewPoint_str=str(myDewPoint)
myDateTime_str=str(myDateTime)
myRH_str=str(myRH)
myTemp_str=str(myTemp)
myDevSn_str=str(myDevSn)

insertString="INSERT into airlog.comet"
insertString+=" (measTime_date, measTime, measTime_ts, temperature, dewPoint, rh, devname, devsn, location)"
insertString+=" values (%s, FROM_UNIXTIME(%s), %s, %s, %s, %s, %s, %s, %s)"
myInsertValues = (myDateTime, myUnixTime, myUnixTime, myTemp, myDewPoint, myRH, myDevName, myDevSn, myLocation)
mycursor.execute(insertString, myInsertValues)
mydb.commit()

nInsertedRecords = mycursor.rowcount

if nInsertedRecords != 1 :
    print("ERROR: I found ", mycursor.rowcount, "record inserted instead of 1")

mycursor.close()


